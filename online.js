var request = require('request');
var iconv 	= require('iconv-lite');
var url 	= require('url');

module.exports = {
	sites: {},
	onlines: {},
	getSitesConfig: function(callback){
		console.log('call getSitesConfig');
		var check_url = 'http://ivona.bigmir.net?_ctr=article.getSitesConfig';
		var t = this;
		request({uri: check_url}, function (error, response, body) {

			if (!error && response.statusCode == 200) {
				var result = JSON.parse(body);
				if(result)
				{
					console.log('getSitesConfig success. config saved');
					t.sites = result;
					callback();
				}else{
					setTimeout(function(){this.getSitesConfig(callback)},5000);
				}

			}else{
				console.log('error in fetching url'+error);
			}
		});
	},
	buildCheckUrl: function(site_id,page_id,postfix){
		if(typeof this.sites[site_id] == 'undefined')
			return false;

		return this.sites[site_id] + postfix+'&page_id='+page_id;
	},
	checkIsLive: function(io){
		var forceSingleCheck = null;
		console.log('checkIsLive');

		if (typeof arguments[1] != 'undefined' )
			forceSingleCheck = arguments[1];

		console.log('forceSingleCheck',forceSingleCheck);

		if(Object.keys(this.onlines).length>0)
		{
			var i,check_url;
			for(i in this.onlines)
			{
				if( forceSingleCheck != null && forceSingleCheck != i ) // ms
				{
	//				console.log('skipped',online);
					continue;
				}

				if( typeof this.onlines[i] != 'undefined' && typeof this.onlines[i].lastCheck != 'undefined' && (Date.now()-this.onlines[i].lastCheck<2*60*1000) )
					continue;

				console.log('check online ', i);

				check_url = this.onlines[i].url;

				var t = this;
				var makeCheck = function (id){
					var online = id;

					request({uri: check_url,qs:''}, function (error, response, body) {
						console.log('request ok: '+ online);
						if (!error && response.statusCode == 200) {
							var result = JSON.parse(body);
							console.log('result',result,online);
							console.log('onlines',t.onlines);

							if(result.live)
							{
								console.log('auto Live '+online+' check. Live is on. ');
								t.onlines[online] = {state: 'live',url: check_url,lastCheck:Date.now()};
							}else{

								if (typeof t.onlines[online] != 'undefined' && typeof t.onlines[online].lastCheck != 'undefined' && (Date.now()-t.onlines[online].lastCheck>40*60*1000))
								{
									console.log('auto Live '+online+' check. Live is old - delete. ' +online);
									delete t.onlines[online];
								}else{
									console.log('auto Live '+online+' check. Live is off. '+online);
									t.onlines[online] = {state: 'off',url: check_url,lastCheck:Date.now()};
								}
								io.sockets.clients('page_'+online).forEach(function(client){
									client.disconnect();
								});

							}

						}else{
							console.log('error in fetching url'+error);
						}
					});
				}
				makeCheck(i);
			}
		}else
			console.log('there is no active onlines');
	},
	http_total_connected: function(req,response,connectCounter)
	{
		console.log('total_connected ' + connectCounter);
		response.writeHead(200, {'Content-Type': 'text/html'});
		response.write( 'res ' +connectCounter );
		response.end();
	},
	http_online_start: function(req,response,urlPostfix)
	{
		console.log('http_online_start');
		var query = url.parse(req.url,true).query;
		var page_id = query.page_id;
		var site_id = query.site_id;
		var public_url = this.buildCheckUrl(site_id,page_id,urlPostfix);

		this.onlines[page_id] = {state:'live',url: public_url,site_id:site_id};

		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write( JSON.stringify({status:'ok start live' + page_id}) );
		response.end();
	},
	http_online_end: function(req,response,io,urlPostfix)
	{
		var query = url.parse(req.url,true).query;
		var page_id = query.page_id;
		var site_id = query.site_id;
		var public_url = this.buildCheckUrl(site_id,page_id,urlPostfix);
		if (page_id)
		{
			this.onlines[page_id] = {state:'off',url: public_url,site_id:site_id};

			io.sockets.clients('page_'+page_id).forEach(function(client){
				client.disconnect();
			});


		}
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write( JSON.stringify({status:'ok disconnected live '+page_id}) );
		response.end();
	},
	http_online_delete: function(req,response,io)
	{
		var query = url.parse(req.url,true).query;
		var page_id = query.page_id;
		var hash = query.hash;
		var modified = query.modified;
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write( JSON.stringify({status:'ok'}) );
		response.end();
		io.sockets.to('page_'+page_id).emit('public_message', {'content': '',hash:hash, action: 'delete', modified:modified});
	},
	http_online_update: function(req,response,io)
	{
		//			console.log('online_update');

		var query = url.parse(req.url,true).query;
		var page_id = query.page_id;
		var public_url = query.url;
		var modified = query.modified;
		var hash = query.hash;

		if (page_id)
		{
			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write( JSON.stringify({status:'ok'}) );
			response.end();

//				console.log('online update',page_id);
			request({uri: public_url,encoding: 'binary'}, function (error, response, body) {

				body = iconv.decode(body, 'win1251');

				if (!error && response.statusCode == 200) {
//						console.log('uor body');
//						console.log(body);
					if(body)
						io.sockets.to('page_'+page_id).emit('public_message', {'content': body, modified: modified, hash: hash});
				}else{
						console.log('error in fetching url'+error);
				}
			})
		}
	},
	on_page_init: function(data,socket,io,urlPostfix)
	{
		console.log('page_init',data);

		if(typeof data['page_id'] == 'undefined')
		{
			console.log('Bad args. disconnect',data);
			socket.disconnect();
			return ;
		}

		if(typeof(this.onlines[data['page_id']]) == 'undefined')
		{
			console.log('unknown online. add to stack for check: ' + data['page_id']);
			var check_url  = this.buildCheckUrl(data['site_id'],data['page_id'],urlPostfix);
			this.onlines[data['page_id']] = {state: 'live',url: check_url,site_id: data['site_id']};
			var t=this;
			setTimeout(function(){
				t.checkIsLive(io,data['page_id']);
			},parseInt(Date.now().toString().substr(-1)/3)*1000);
		}

		if(  typeof(this.onlines[data['page_id']]) != 'undefined' && this.onlines[data['page_id']].state == 'live')
		{
//			console.log('online is on. subscribe to channel');
			socket.pageId = data['page_id'];
			socket.join('page_'+socket.pageId); // split all clients by pages
		}else if(typeof(this.onlines[data['page_id']]) != 'undefined' && this.onlines[data['page_id']].state == 'off'){
			console.log(data['page_id'] + ' - disconnected due to page online live is off');
			socket.disconnect();
		}

	},
	http_conference_update: function(req,response,io)
	{
		var query = url.parse(req.url,true).query;
		var page_id = query.page_id;
		console.log('req.url',req.url);

		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write( JSON.stringify({status:'ok'}) );
		response.end();
		io.sockets.to('page_'+page_id).emit('public_message', {data:query});
	}


}