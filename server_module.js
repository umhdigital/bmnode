var http 	= require("http");
var url 	= require('url');
var request = require('request');

var connectCounter = 0;
//qwe2

var online = require('./online');
online.getSitesConfig(init);

function init()
{

	var server = http.createServer(function(req, response){
		var path = url.parse(req.url).pathname;

		switch(path){

			case '/total_connected':
				online.http_total_connected(req, response,connectCounter);
				break;

			case '/online_start':
				online.http_online_start(req, response,'?_ctr=article.isLiveOn');
				break;

			case '/online_end':
				online.http_online_end(req, response,io,'?_ctr=article.isLiveOn');
				break;

			case '/online_delete':
				online.http_online_delete(req, response,io);
				break;

			case '/online_update':
				online.http_online_update(req, response,io);
				break;

			case '/conference_finished':
				online.http_online_end(req, response,io,'?_ctr=ugc.getState&ajax=1');
				break;

			case '/conference_save':
			case '/conference_delete':
				console.log(path + ' action');

				online.http_conference_update(req, response,io);
				break;


			default:
				response.writeHead(404);
				response.write("opps this doesn't exist - 404. Path: " + path);
				response.end();
				break;
		}

	});

	var port = 8099;
	if (typeof process.env.BMNODE_PORT !== 'undefined') port = process.env.BMNODE_PORT;

	server.listen(port);


	var io = require('socket.io').listen(server);
	io.set('log level', 1);
	io.set("timeout", 120);
	io.set("close timeout", 120);

	io.sockets.on('connection', function(socket){
		connectCounter++;

		socket.on('disconnect', function(data) {
			connectCounter--;
		});

		socket.on('error',function(data){
//		console.log('error occured',data);
		});

		socket.on('page_init',function(data)
		{
			online.on_page_init(data,socket,io,'?_ctr=article.isLiveOn');
			if(typeof data.test != 'undefined')
				console.log(socket);
		});

		socket.on('conference_init',function(data)
		{
			console.log('conference_init',data);
			online.on_page_init(data,socket,io,'?_ctr=ugc.getState&ajax=1');
		});

	});

	setInterval(function(){
		online.checkIsLive(io);
	},60*60*1000); // each hour recheck state

}

